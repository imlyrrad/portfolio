const routes = [
	{
		path: '/',
		component: () => import('layouts/Main.vue'),
		children: [
			{
				path: '',
				name: 'index',
				component: () => import('pages/Index.vue'),
			},
			{
				path: 'about',
				name: 'about-me',
				component: () => import('pages/About.vue'),
			},
			{
				path: 'work-experience',
				name: 'experience',
				component: () => import('pages/Work.vue'),
			},
			{
				path: 'skills',
				name: 'skills',
				component: () => import('pages/Skills.vue'),
			},
			{
				path: 'contact',
				name: 'contact',
				component: () => import('pages/Contact.vue'),
			},
			{
				path: 'covid-19',
				name: 'covid-19',
				component: () => import('pages/assets/Covid-19.vue'),
			},
			{
				path: 'pro-mon',
				name: 'pro-mon',
				component: () => import('pages/Pro-mon.vue'),
			},
			{
				path: 'readme',
				name: 'README.md',
				component: () => import('pages/Readme.vue'),
			},
		],
	},
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
	routes.push({
		path: '*',
		component: () => import('pages/Error404.vue'),
	})
}

export default routes
