import IndexComponent from "./../../components/index";
import ContactComponent from "./../../components/contact";
import AboutMeComponent from "./../../components/about-me";
import SkillSetsComponent from "./../../components/skill-sets";

export default {
	name: "MainLayout",

	components: {
		IndexComponent,
		ContactComponent,
		AboutMeComponent,
		SkillSetsComponent,
	},

	data() {
		return {
			selected: this.$route.name,
			left: false,
			expanded: ["portfolio", "src", "assets", "pages"],
			simple: [
				{
					label: "portfolio",
					icon: "ion-ios-folder-open",
					children: [
						{
							label: "src",
							icon: "ion-ios-folder-open",
							children: [
								{
									label: "assets",
									icon: "ion-ios-folder-open",
									children: [
										{
											label: "covid-19",
											icon: "ion-ios-apps",
										},
										{
											label: "pro-mon",
											icon: "ion-ios-desktop",
										},
									],
								},
								{
									label: "pages",
									icon: "ion-ios-folder-open",
									children: [
										{
											label: "about-me",
											icon: "ion-ios-document",
										},
										{
											label: "contact",
											icon: "ion-ios-document",
										},
										{
											label: "experience",
											icon: "ion-ios-document",
										},
										{
											label: "skills",
											icon: "ion-ios-document",
										},
									],
								},
								{
									label: "index",
									icon: "ion-ios-code",
								},
							],
						},

						{
							label: "README.md",
							icon: "ion-ios-document",
						},
					],
				},
			],

			previousSelected: this.$route.name,
		};
	},

	watch: {
		selected(value) {
			if (value) {
				let folders = [
					"portfolio",
					"src",
					"assets",
					"pages",
					"uploads",
				];

				/** verify user clicks */
				if (folders.filter((f) => f === value).length) {
					/** Set Default Active Link */
					this.selected = this.$route.name;
					/** Check if already expanded then minimize it */
					if (this.expanded.filter((f) => f === value).length) {
						let index = this.expanded.findIndex((e) => e === value);
						index !== -1 && this.expanded.splice(index, 1);
					} else {
						this.expanded.push(value);
					}
				} else {
					/** Navigate Router */
					value !== this.$route.name &&
						this.$router.push({ name: value });
				}
			} else {
				this.selected = this.$route.name;
			}
		},
	},

	methods: {},
};
